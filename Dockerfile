# FROM python:alpine3.7
FROM faizanbashir/python-datascience:3.6
RUN mkdir /app
COPY ./requirements.txt /app
# COPY ./matplotlib-3.1.1.tar.gz /app
WORKDIR /app
## RUN echo "http://dl-4.alpinelinux.org/alpine/edge/community" >> /etc/apk/repositories
## RUN apk --update add --no-cache \ 
##     libgfortran \
##     lapack-dev \ 
##     gcc \
##     freetype-dev
## 
## RUN apk add subversion
## 
## # RUN apk add python3 py-pip python3-dev 
## 
## # Install dependencies
## RUN apk add --no-cache --virtual .build-deps \
##     gfortran \
##     musl-dev \
##     g++
## # RUN ln -s /usr/include/locale.h /usr/include/xlocale.h 
## # RUN ln -fs /usr/bin/python3 /usr/local/bin/python 
## # RUN ln -fs /usr/local/bin/pip3 /usr/local/bin/pip
## 
RUN pip3 install -r requirements.txt
# RUN pip3 install ./matplotlib-3.1.1.tar.gz
EXPOSE 4000
CMD python ./app.py
